import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { HttpClient } from '@angular/common/http';

import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  selectedFile: File;
  selectedCountries: any;
  country: any;
  loading: boolean;
  model: any = {};
  httpClient: any;
  imgUrl: any;
  imageUploded = false;
 

  constructor(private service: ServicesService, private http: HttpClient,
              private router: Router, private toastr: ToastrService) { }

  ngOnInit() {

  }


  onFileChanged(event) { 

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.selectedFile = event.target.files[0];

      console.log(this.selectedFile);

      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (event: any) => {
          this.imgUrl = event.target.result;
          this.imageUploded = true;
      };

      reader.readAsDataURL(event.target.files[0]);
  }
}

  onSubmit(f: NgForm) {
    console.log(this.selectedFile );
    if (this.selectedFile === undefined) {
      this.toastr.error('Failed', 'Add your profile image!');
      return;
    }

    this.loading = true;

    const formData = new FormData();
    formData.append('image', this.selectedFile, this.selectedFile.name);
    formData.append('firstName', this.model.firstName);
    formData.append('lastName', this.model.lastName);
    formData.append('previous_surname', this.model.previous_surname);
    formData.append('date_of_birth', this.model.date_of_birth);
    formData.append('gender', this.model.gender);
    formData.append('birth_place', this.model.birth_place);
    formData.append('nationality', this.model.nationality);
    formData.append('pam_address', this.model.pam_address);
    formData.append('address_country', this.model.address_country);
    formData.append('current_mobile', this.model.current_mobile);
    formData.append('email_address', this.model.email_address);
    formData.append('employer', this.model.employer);
    formData.append('license_from', this.model.license_from);
    formData.append('license_to', this.model.license_to);

    // http://ncaaame.africacodes.net
    // http://localhost:8888/email/index.php/Mailservices/addLicense

    this.http.post('http://localhost:8888/email/index.php/Mailservices/addLicense', formData ).subscribe(
        (data) => {
          this.toastr.success('Data Saved!', 'Saved !');
          this.loading = false;
          f.resetForm();
          this.router.navigate(['/thank']);
      }, (error) => {
        this.loading = false;
        this.toastr.error('Failed', 'Unable to save Data !');
    });
}


}
