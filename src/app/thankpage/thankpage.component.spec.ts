import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankpageComponent } from './thankpage.component';

describe('ThankpageComponent', () => {
  let component: ThankpageComponent;
  let fixture: ComponentFixture<ThankpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThankpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
